
/**
 * @flow this view is used to js puzzle4 
 */

function calculateFactorial(number) {
//this will convert the value integer when you pass the value as string
    let givenNumber=parseInt(number);
//check the given number ==0 it will execute
    if(givenNumber == 0) {
        return 1;
    }
//if given number less then zero this will execute
    else if(givenNumber < 0 ) {
        return undefined;
    }
    //here we took the for loop it explains the loop the given value with "-1"
    for(var  i = givenNumber; --i;) {
     
        givenNumber =givenNumber * i;
    
        console.log(i);
    }
//returned the value .
    return givenNumber;
}
console.log(calculateFactorial(5));
