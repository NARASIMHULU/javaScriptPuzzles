/**
 * @flow this view is used to js puzzle3 
 */

  function removecharecter(inputData){
      //it takes the parameter input data.
    let inputString=inputData;
    let  updatedString='';
    //loping the  input string 
    for (var x=0;x < inputString.length;x++){
        //indexOf refers the checking the position of the value. 
        //and removing the repeated charecter and inserted in to updated string
        if(updatedString.indexOf(inputString.charAt(x))==-1){
            updatedString = updatedString+inputString[x];  
        }
    }
    // get the final result from the return statement.
    return updatedString;
  }
  //giving the input as string.
  console.log(removecharecter("aabbccddeeffgghh"));
